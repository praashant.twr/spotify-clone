from app import app, db, celery
from flask import render_template, flash, redirect, url_for, request, g, send_file
from app.forms import LoginForm, SignUpForm, EditProfileForm, ResetPasswordRequestForm, ResetPasswordForm, ChangePasswordForm, AddMusic, SearchForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Music
import datetime
import os
from werkzeug.security import generate_password_hash, check_password_hash
from app.email import send_password_reset_email


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Spotify')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('profile', id=user.id))
    return render_template('login.html', title='Spotify', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = SignUpForm()
    if form.validate_on_submit():
        year = form.year.data
        date = form.date.data
        month = form.month.data
        dob = str(date) + '/' + month + '/' + str(year)
        # print('credentials = ', dob, form.username.data, form.email.data, form.gender.data)
        try:
            datetime.datetime.strptime(dob, '%d/%m/%Y')
            today = datetime.date.today()
            age = abs(year - today.year)
            if age < 18:
                flash('Your age is less then the required age to use Spotify')
                return redirect(url_for('signup'))
        except:
            flash('date error')
            return redirect(url_for('signup'))
        user = User(username=form.username.data,
                    email=form.email.data, gender=form.gender.data, dob=dob)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('signup.html', title='Register', form=form)


@app.route('/profile/<id>')
@login_required
def profile(id):
    user = User.query.filter_by(id=id).first_or_404()
    return render_template('profile.html', user=user)


@app.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditProfileForm()
    dob = current_user.dob
    date_of_birth = dob.split('/')
    # print(date_of_birth)
    print(form.password.data)
    if form.validate_on_submit():
        if not current_user.check_password(form.password.data):
            flash('Invalid password')
            return redirect(url_for('edit'))
        year = form.year.data
        month = form.month.data
        date = form.date.data
        new_dob = str(date) + '/' + month + '/' + str(year)
        try:
            datetime.datetime.strptime(new_dob, '%d/%m/%Y')
            today = datetime.date.today()
            age = abs(year - today.year)
            if age < 18:
                flash('Your age is less then the required age to use Spotify')
                return redirect(url_for('edit'))
        except:
            flash('date error')
            return redirect(url_for('edit'))
        current_user.dob = new_dob
        current_user.gender = form.gender.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit'))
    elif request.method == 'GET':
        form.email.data = current_user.email
        form.year.data = date_of_birth[2]
        form.month.data = date_of_birth[1]
        form.date.data = date_of_birth[0]
        form.gender.data = current_user.gender
    return render_template('edit.html', title='Edit Profile - Spotify', form=form)


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title='Reset Password', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)


@app.route('/change_password', methods=['POST', 'GET'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if not current_user.check_password(form.current_password.data):
            flash('Invalid Current password')
            return redirect(url_for('change_password'))
        current_user.set_password(form.new_password.data)
        db.session.commit()
        flash('Password Reset Complete.')
        return redirect(url_for('change_password'))
    print('error = ', form.errors)
    return render_template('change_password.html', form=form, title='Change your password - Spotify')


@app.route('/admin')
def admin():
    form = AddMusic()
    if form.validate_on_submit():
        music = Music(name=form.name.data, genre=form.genre.data, cover_img_location=form.cover_img.data,
                      song_file_location=form.song_file_location.data, artist=form.artist.data)
        db.session.add(music)
        db.session.commit()
        flash('Song added Successfully')
    return render_template('add_music.html', form=form)


@app.route('/player')
def player():
    # songs = os.listdir('app/static/music')
    g.search_form = SearchForm()
    songs = Music.query.all()

    if g.search_form.validate():
        page = request.args.get('page', 1, type=int)
        music, total = Music.search(g.search_form.q.data, page, 10)
        return render_template('player.html', music=music, songs=songs, search_form=g.search_form)
    return render_template('player.html', songs=songs, search_form=g.search_form)


@app.route('/download', methods=['GET', 'POST'])
def download():
    song = 'abc'
    # if current_user.is_authenticated:
    #     print(url_for('static', filename=song.song_file_location))
    #     return send_file('/home/prashant/Downloads', attachment_filename='static/song_img/cover.jpg')
    return render_template('download.html', song=song)

@celery.task
def add(a, b):
    return a + b
