from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, SelectField, RadioField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Length
from app.models import User
from flask import request


class LoginForm(FlaskForm):
    email = StringField('email', render_kw={"placeholder": " Email or username"}, validators=[DataRequired(), Email()])
    password = PasswordField('Password', render_kw={"placeholder": " Password"}, validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In', render_kw={"class": "btn btn-success rounded-pill"})

class SignUpForm(FlaskForm):
    month_selector = [('0','Months'), ('01','January'), ('02','February'), ('03','March'), ('04','April'), ('05', 'May'), ('06', 'June'),('07', 'July'),('08', 'August'),('09', 'September'), ('10', 'October'),('11', 'November'),('12', 'December')]
    email = StringField('Email', render_kw={"placeholder": " Email"}, validators=[DataRequired(), Email()])
    email2 = StringField('Confirm email', render_kw={"placeholder": " Confirm Email"}, validators=[DataRequired(), Email(), EqualTo('email')])
    password = PasswordField('Password', render_kw={"placeholder": " Password"}, validators=[DataRequired()])
    username = StringField('username', render_kw={"placeholder": " What should we call you ?"}, validators=[DataRequired()])
    year = IntegerField('year', render_kw={"placeholder": " Year"}, validators=[DataRequired()])
    month = SelectField('month', render_kw={"class": "custom-select"}, choices=month_selector, validators=[DataRequired()])
    date =  IntegerField('date', render_kw={"placeholder": " Date"}, validators=[DataRequired()])
    submit = SubmitField('Sign Up', render_kw={"class": "btn btn-success btn-lg btn-block rounded-pill"})
    gender = RadioField('gender', render_kw={"class": "form-check form-check-inline", "style": "list-style: none;"}, choices=[('m', 'Male'), ('f', 'Female'), ('n', 'Non-binary')])

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class EditProfileForm(FlaskForm):
    month_selector = [('0','Months'), ('01','January'), ('02','February'), ('03','March'), ('04','April'), ('05', 'May'), ('06', 'June'),('07', 'July'),('08', 'August'),('09', 'September'), ('10', 'October'),('11', 'November'),('12', 'December')]
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Confirm password', validators=[DataRequired()])
    gender = SelectField('gender', choices=[('m', 'Male'), ('f', 'Female'), ('n', 'Non-binary')])
    year = IntegerField('year', validators=[DataRequired()])
    month = SelectField('month', choices=month_selector, validators=[DataRequired()])
    date =  IntegerField('date', validators=[DataRequired()])
    submit = SubmitField('Save Profile', render_kw={"class": "btn btn-success rounded-pill"})
    cancil = SubmitField('Cancil')

class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('    SEND    ', render_kw={"class": "btn btn-success rounded-pill"})

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Password Reset')

class ChangePasswordForm(FlaskForm):
    current_password = PasswordField('Current Password', validators=[DataRequired()])
    new_password = PasswordField('New Password', validators=[DataRequired()])
    new_password2 = PasswordField(
        'Repeat New Password', validators=[DataRequired(), EqualTo('new_password')])
    submit = SubmitField('Set New Password', render_kw={"class": "btn btn-success rounded-pill"})

class AddMusic(FlaskForm):
    name = StringField('Song Name', render_kw={"placeholder": " Song Name"}, validators=[DataRequired()])
    genre = StringField('Genre', render_kw={"placeholder": " Genre"}, validators=[DataRequired()])
    cover_img = StringField('Cover Location', render_kw={"placeholder": " Add Cover Image"}, validators=[DataRequired()])
    song_file_location = StringField('Song Location', render_kw={"placeholder": " Add Song File"}, validators=[DataRequired()])
    artist = StringField('Artist', render_kw={"placeholder": " Artist"}, validators=[DataRequired()])
    submit = SubmitField('Add song', render_kw={"class": "btn btn-success rounded-pill"})

class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)