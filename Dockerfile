FROM python:3.6-alpine

RUN adduser -D spotify

WORKDIR /home/spotify

COPY requirements.txt requirements.txt
RUN python3 -m venv spotifyenv
RUN spotifyenv/bin/pip install -r requirements.txt
RUN spotifyenv/bin/pip install gunicorn
RUN spotifyenv/bin/pip install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY spotify.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP spotify.py

RUN chown -R spotify:spotify ./
USER spotify

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]