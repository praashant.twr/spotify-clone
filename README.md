
-------------------------------------------------------- DOCKER -------------------------------------------------------------

sudo docker run --name mysql -d -e MYSQL_RANDOM_ROOT_PASSWORD=yes -e MYSQL_DATABASE=spotify -e MYSQL_USER=spotify -e MYSQL_PASSWORD=pass mysql/mysql-server:5.7

sudo docker build -t spotify:latest .

sudo docker run --name spotify -d -p 8000:5000 --rm -e SECRET_KEY=try_to_guess -e MAIL_SERVER=smtp.googlemail.com -e MAIL_PORT=587 -e MAIL_USE_TLS=true -e MAIL_USERNAME=<username> -e MAIL_PASSWORD=<password> --link mysql:dbserver -e DATABASE_URL=mysql+pymysql://spotify:pass@dbserver/spotify spotify:latest

sudo docker stop <key>

sudo docker login

sudo docker tag spotify:latest docker101docker/spotify:latest

sudo docker push docker101docker/spotify:latest

---------------------------------------------------------- AWS ----------------------------------------------------------------------

cd to downloads

ssh -i "spotify-clone.pem" ubuntu@ec2-52-14-77-1.us-east-2.compute.amazonaws.com

--------------------------------------------------------- CELERY -------------------------------------------------------------------

celery -A app.celery worker --loglevel=info
service redis start/status/stop